package singleton;

import java.util.Date;

public class MinerRobotFactory implements RobotFactoryInterface {

    @Override
    public Robot makeRobot(String name) {
	MinerRobot robot = new MinerRobot();
	robot.setName(name);
	robot.setDateCreated(new Date());
	robot.setRole(Role.MINER);
	return robot;
    }

}