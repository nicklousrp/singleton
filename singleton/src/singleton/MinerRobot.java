package singleton;

public class MinerRobot extends Robot {

    @Override
    public void showSkill() {
	System.out
		.println("Harvesting resources to keep you cool in the summer and warm in the winter.");
    }

    @Override
    public void doWork() {
	System.out.println("Managing energy resources.");
    }

}
