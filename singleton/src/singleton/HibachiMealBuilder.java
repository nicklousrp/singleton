package singleton;

public class HibachiMealBuilder extends MealBuilder {

	@Override
    public void buildMainCourse() {
	meal.setMainCourse("Geisha Special");
    }

    @Override
    public void buildDrink() {
	meal.setDrink("Sake");
    }

    @Override
    public void buildDessert() {
	meal.setDessert("Pound Cake");
    }

    @Override
    public void buildSideDish() {
	meal.setSideDish("Fried Rice");
    }

    @Override
    public Meal getMeal() {
	return meal;
    }

}