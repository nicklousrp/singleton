package singleton;

import java.util.Date;

public class MultiPurposeRobotFactory implements RobotFactoryInterface {

    @Override
    public Robot makeRobot(String name) {
	MultiPurposeRobot robot = new MultiPurposeRobot();
	robot.setName(name);
	robot.setDateCreated(new Date());
	robot.setRole(Role.MULTIPURPOSE);

	return robot;
    }

}
