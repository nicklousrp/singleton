package singleton;

public interface RobotFactoryInterface {
	 public Robot makeRobot(String name);
}
