package singleton;

public class RobotFactory implements RobotFactoryInterface {
    private Role role;

    public void setRole(Role role) {
	this.role = role;
    }

    @Override
    public Robot makeRobot(String name) {
	Robot robot = null;

	if (role == Role.CHEF) {
	    robot = new ChefRobotFactory().makeRobot(name);
	} else if (role == Role.GARDENER) {
	    robot = new GardenerRobotFactory().makeRobot(name);
	} else if (role == Role.MINER) {
	    robot = new MinerRobotFactory().makeRobot(name);
	} else if (role == Role.TEACHER) {
	    robot = new TeacherRobotFactory().makeRobot(name);
	} else if (role == Role.MULTIPURPOSE) {
	    robot = new MultiPurposeRobotFactory().makeRobot(name);
	} else if (role == Role.BUTLER) {
	    robot = new ButlerRobotFactory().makeRobot(name);
	}

	return robot;
    }
}
