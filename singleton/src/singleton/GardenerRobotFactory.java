package singleton;

import java.util.Date;

public class GardenerRobotFactory implements RobotFactoryInterface {

    @Override
    public Robot makeRobot(String name) {
	GardenerRobot robot = new GardenerRobot();
	robot.setName(name);
	robot.setDateCreated(new Date());
	robot.setRole(Role.GARDENER);
	return robot;
    }
}
