package singleton;

public class ItalianMealBuilder extends MealBuilder {

	@Override
    public void buildMainCourse() {
	meal.setMainCourse("Pasta al Forno");
    }

    @Override
    public void buildDrink() {
	meal.setDrink("Adams Reserve Pinot Noir");
    }

    @Override
    public void buildDessert() {
	meal.setDessert("Tiramisu");
    }

    @Override
    public void buildSideDish() {
	meal.setSideDish("Italian Wedding Soup");
    }

    @Override
    public Meal getMeal() {
	return meal;
    }

}