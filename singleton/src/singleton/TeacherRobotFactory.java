package singleton;

import java.util.Date;

public class TeacherRobotFactory implements RobotFactoryInterface {

    @Override
    public Robot makeRobot(String name) {
	TeacherRobot robot = new TeacherRobot();
	robot.setName(name);
	robot.setDateCreated(new Date());
	robot.setRole(Role.TEACHER);
	return robot;
    }
}