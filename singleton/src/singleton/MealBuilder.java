package singleton;

public abstract class MealBuilder {
    protected Meal meal = new Meal();

    public abstract void buildMainCourse();

    public abstract void buildDrink();

    public abstract void buildDessert();

    public abstract void buildSideDish();

    public abstract Meal getMeal();
}
