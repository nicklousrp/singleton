package singleton;

import java.util.Date;

public class ChefRobotFactory implements RobotFactoryInterface {

    @Override
    public Robot makeRobot(String name) {
	ChefRobot robot = new ChefRobot();
	robot.setName(name);
	robot.setDateCreated(new Date());
	robot.setRole(Role.CHEF);

	return robot;
    }
}