package singleton;

public class GardenerRobot extends Robot {

    @Override
    public void showSkill() {
	System.out.println("Manicuring your lawn to 99.999% perfection!");
    }

    @Override
    public void doWork() {
	System.out.println("Mowing the lawn and trimming the hedges.");
    }
}
