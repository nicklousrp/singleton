package singleton;

public class MealDirector {
    public Meal createMeal(MealBuilder builder) {
	builder.buildMainCourse();
	builder.buildSideDish();
	builder.buildDessert();
	builder.buildDrink();
	return builder.getMeal();
    }
}