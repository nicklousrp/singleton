package singleton;

import java.util.Random;

public class ChefRobot extends Robot {
    @Override
    public void showSkill() {
	System.out
		.println("Would you like a steak for dinner? Or how about some hibachi?");
    }

    @Override
    public void doWork() {
	MealDirector director = new MealDirector();
	MealBuilder builder = null;
	Integer selection = new Random().nextInt(3) + 1;

	if (selection == 1) {
	    builder = new ItalianMealBuilder();
	} else if (selection == 2) {
	    builder = new HibachiMealBuilder();
	} else {
	    builder = new SteakMealBuilder();
	}

	Meal meal = director.createMeal(builder);
	System.out.println(meal);
    }
}