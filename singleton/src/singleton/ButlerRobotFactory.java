package singleton;

import java.util.Date;

public class ButlerRobotFactory implements RobotFactoryInterface {

    @Override
    public Robot makeRobot(String name) {
	ButlerRobot robot = new ButlerRobot();
	robot.setName(name);
	robot.setDateCreated(new Date());
	robot.setRole(Role.BUTLER);
	return robot;
    }

}
