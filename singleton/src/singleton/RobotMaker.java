package singleton;

public class RobotMaker {
	private static RobotFactory FACTORY = null;
	
	public synchronized static RobotFactory getInstance () {
			if (FACTORY == null) {
				FACTORY = new RobotFactory ();
				
			}
			return FACTORY;
	}
}
