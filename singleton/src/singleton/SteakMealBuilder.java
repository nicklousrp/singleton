package singleton;

public class SteakMealBuilder extends MealBuilder {
	 @Override
	    public void buildMainCourse() {
		meal.setMainCourse("Porterhouse, Medium Well");
	    }

	    @Override
	    public void buildDrink() {
		meal.setDrink("Beer");
	    }

	    @Override
	    public void buildDessert() {
		meal.setDessert("Texas Cheesecake");
	    }

	    @Override
	    public void buildSideDish() {
		meal.setSideDish("Ceasers Salad");
	    }

	    @Override
	    public Meal getMeal() {
		return meal;
	    }
	}