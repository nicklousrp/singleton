package singleton;

public enum Role {
	TEACHER, CHEF, MINER, GARDENER, MULTIPURPOSE, BUTLER
}
