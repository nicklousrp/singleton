package singleton;

public class TeacherRobot extends Robot {

    @Override
    public void showSkill() {
	System.out.println("5 + 3 = 8 and the capital of Japan is Tokyo.");
    }

    @Override
    public void doWork() {
	System.out.println("Teaching Algebra.");
    }
}
